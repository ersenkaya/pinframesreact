export default class BlogItem {
  constructor({ blogImage, blogId, blogTitle, blogSubtitle }) {
    this.blogImage = blogImage;
    this.blogId = blogId;
    this.blogTitle = blogTitle;
    this.blogSubtitle = blogSubtitle;
  }
}
