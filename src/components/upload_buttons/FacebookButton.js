import React, { useRef } from "react";
import { Widget } from "@uploadcare/react-widget";
import CustomButton from "../CustomButton";
import strings from "../../helpers/Localization";
const FacebookButton = () => {
  const widgetApi = useRef();
  return (
    <>
      <div style={{ display: "none" }}>
        <Widget tabs="facebook" tabsCss="https://site.com/styles/uc.tabs.css" />
        <Widget ref={widgetApi} tabs="facebook" publicKey="demopublickey" />
      </div>
      <CustomButton
        onClick={() => widgetApi.current.openDialog()}
        imgSrc="../facebook.png"
        title={strings.load_with_facebook}
      />
    </>
  );
};

export default FacebookButton;
