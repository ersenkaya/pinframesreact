import React, { useRef } from "react";
import { Widget } from "@uploadcare/react-widget";
import CustomButton from "../CustomButton";
import strings from "../../helpers/Localization";
const InstagramButton = () => {
  const widgetApi = useRef();
  return (
    <>
      <div style={{ display: "none" }}>
        <Widget
          tabs="instagram"
          tabsCss="https://site.com/styles/uc.tabs.css"
        />
        <Widget ref={widgetApi} tabs="instagram" publicKey="demopublickey" />
      </div>
      <CustomButton
        onClick={() => widgetApi.current.openDialog()}
        imgSrc="../instagram.png"
        title={strings.load_with_instagram}
      />
    </>
  );
};

export default InstagramButton;
