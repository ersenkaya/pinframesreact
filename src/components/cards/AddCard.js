import { Col, Row, Space, Divider } from "antd";
import React, { useRef } from "react";
import strings from "../../helpers/Localization";
import AddIcon from "./../icons/AddIcon";
import { Widget } from "@uploadcare/react-widget";
function AddCard(props) {
  const widgetApi = useRef();
  const widgetApiFacebook = useRef();
  const widgetApiInstagram = useRef();

  const fileSelect = (file) => {
    console.log("File changed: ", file);

    if (file) {
      file.progress((info) => console.log("File progress: ", info.progress));
      file.done((info) => {
        console.log("File uploaded: ", info);
        props.loadImage(info);
      });
    }
  };

  return (
    <Col span={8}>
      <div style={{ display: "none" }}>
        <Widget
          ref={widgetApi}
          tabs="file"
          publicKey="demopublickey"
          onFileSelect={fileSelect}
          onChange={(info) => console.log("Upload completed:", info)}
        />
        <Widget
          ref={widgetApiFacebook}
          tabs="facebook"
          publicKey="demopublickey"
          onFileSelect={fileSelect}
          onChange={(info) => console.log("Upload completed:", info)}
        />
        <Widget
          ref={widgetApiInstagram}
          tabs="instagram"
          publicKey="demopublickey"
          onFileSelect={fileSelect}
          onChange={(info) => console.log("Upload completed:", info)}
        />
      </div>
      <Row
        style={{
          width: 240,
          height: 240,
          borderRadius: 8,
          background: "white",
          boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.1)",
        }}
        className="area"
        onClick={props.clickDetail}
        justify="center"
        align="middle"
      >
        <Col
          style={{
            textAlign: !props.isClickChoice ? "center" : "left",
            width: 240,
          }}
        >
          {!props.isClickChoice ? (
            <>
              {" "}
              <AddIcon size="24" fill="#FF6600" />
              <div style={{ color: "#FF6600", fontSize: 16 }}>
                {" "}
                {strings.load_photo}
              </div>
            </>
          ) : (
            <>
              <div onClick={() => widgetApi.current.openDialog()}>
                <Space wrap style={{ paddingLeft: 24 }}>
                  <div style={{ width: 16, height: 24, paddingTop: 4 }}>
                    <AddIcon size="22" fill="#FF6600" />
                  </div>
                  <div style={{ color: "#FF6600", fontSize: 16 }}>
                    {" "}
                    {strings.load_photo}
                  </div>
                </Space>
              </div>
              <Divider type="horizontal" />
              <div onClick={() => widgetApiInstagram.current.openDialog()}>
                <Space wrap style={{ paddingLeft: 24 }}>
                  <img alt="pinframes" src="../instagram.png" />
                  <div style={{ fontSize: 16, color: "black" }}>
                    {" "}
                    {strings.load_with_instagram}
                  </div>
                </Space>
              </div>
              <Divider type="horizontal" />
              <div onClick={() => widgetApiFacebook.current.openDialog()}>
                <Space wrap style={{ paddingLeft: 24 }}>
                  <img alt="pinframes" src="../facebook.png" />
                  <div style={{ fontSize: 16, color: "black" }}>
                    {" "}
                    {strings.load_with_facebook}
                  </div>
                </Space>
              </div>
            </>
          )}
        </Col>
      </Row>
    </Col>
  );
}

export default AddCard;
